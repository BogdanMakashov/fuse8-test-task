'use strict';

$(document).ready(function(){
    $('.f-reviews').styler({
        selectPlaceholder: 'Filter Reviews',
        selectSmartPositioning : false
    });
    $('.f-sortby').styler({
        selectPlaceholder: 'Most recent',
        selectSmartPositioning : false
    });

    $('.f-sortby').find('.placeholder').prepend('<span>Sort by: </span>');

    (function setRating(){
        var count = new Array();

        $('.customer-experience__details .bar').each(function(index){
            var currentCount =  $(this).data('count');   
            count[index] = currentCount;
            
            var max = getMaxOfArray(count);
            var percent = max / 100;
            
            $(this).find('span').css({'width' : (currentCount / percent) + '%'});
        });
     
        function getMaxOfArray(numArray) {
            return Math.max.apply(null, numArray);
        }
    })();

});
