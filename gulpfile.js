'use sctrict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');

gulp.task('styles', function(){
	return gulp.src('dev/assets/styles/index.scss')
	.pipe(sass({includePaths: require('node-normalize-scss').includePaths}))
	.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
	.pipe(concat('main.css'))
	.pipe(gulp.dest('public/assets/styles'));
});

gulp.task('css', function(){
	return gulp.src('dev/assets/styles/plugins/*.css')
	.pipe(sass({includePaths: require('node-normalize-scss').includePaths}))
	.pipe(gulp.dest('public/assets/styles'));
});


gulp.task('scripts', function(){
	return gulp.src('dev/assets/js/*.*')
	.pipe(gulp.dest('public/assets/js'))
});

gulp.task('images', function(){
	return gulp.src('dev/assets/images/*.*')
	.pipe(gulp.dest('public/assets/img'))
});


gulp.task('assets', function(){
	return gulp.src('dev/index.html')
	.pipe(gulp.dest('public'))
});

gulp.task('clean', function(){
	return del('public');
});

gulp.task('build', gulp.series(
	'clean',
	gulp.parallel('styles', 'css', 'images', 'scripts', 'assets'))
);

gulp.task('watch', function(){
	gulp.watch('dev/assets/styles/*.*', gulp.series('styles'));
	gulp.watch('dev/assets/styles/plugins/*.*', gulp.series('css'));
	gulp.watch('dev/assets/images/*.*', gulp.series('images'));
	gulp.watch('dev/assets/js/*.*', gulp.series('scripts'));
	gulp.watch('dev/*.*', gulp.series('assets'));
});

gulp.task('serve', function(){
	browserSync.init({
		server: 'public'
	});
	browserSync.watch('public/**/*.*').on('change', browserSync.reload);
});

gulp.task('default', gulp.series(
	'build', 
	gulp.parallel('watch', 'serve'))
);



